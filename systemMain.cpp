#include <phpcpp.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <errno.h>
#include<sys/time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include "MainLogic.h"
using namespace std;

int __ImageBase=0;

void sgx_foo(){};
void sgx_bar(){};

extern "C" const struct {
    size_t nr_ecall;    // number of ECALLs
    struct {
        void   *ecall_addr;
        uint8_t is_priv;
    } ecall_table [2];
} g_ecall_table = {2, { { (void*)sgx_foo, 1}, { (void*)sgx_bar, 0} }};

extern "C" const struct {
    // number of OCALLs (number of ECALLs can be found in ECALL table)
    size_t nr_ocall;
    // entry_table[m][n] = 1 iff. ECALL n is allowed in the OCALL m.
    uint8_t entry_table[3][2];
} g_dyn_entry_table =  {
        3, {{0, 0}, {0, 1}, {1, 0}}
};

extern "C" uint32_t sgx_init_crypto_lib(uint64_t cpu_feature_indicator, uint32_t *cpuid_table)
{
    return 0;
}

int sgx_is_within_enclave(const void *addr, size_t size)
{
    return 1;
}

// Mitigator-Public-Key:

MainLogic *mainLogic = NULL;

Php::Value totally_normal_get_mitigator_header()
{
    return mainLogic->get_mitigator_header();
}

void totally_normal_deployment_stage()
{   mainLogic = new MainLogic;
    return mainLogic->deployment_stage();
}

Php::Value totally_normal_php_decrypt_wrapper(Php::Parameters & params)
{
    return mainLogic->php_decrypt_wrapper(params);
}


extern "C" {
    // export the "get_module" function that will be called by the Zend engine
    PHPCPP_EXPORT void *get_module()
    {
        // create extension
        static Php::Extension extension("decryptor_la_setup_and_decryption","1.0");
        extension.onStartup(totally_normal_deployment_stage);
        extension.add<totally_normal_get_mitigator_header>("get_mitigator_header");
        extension.add<totally_normal_php_decrypt_wrapper>("php_decrypt_wrapper",
                                                          {Php::ByVal("array", Php::Type::Array)}
                                                         );
        return extension.module();
    }
}
