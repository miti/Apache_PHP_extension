//
// Created by miti on 2019-12-28.
//

#ifndef APACHE_PHP_EXTENSION_MAINLOGIC_H
#define APACHE_PHP_EXTENSION_MAINLOGIC_H
#include "LA.h"
#include "PostLAMessaging.h"
#include <string>
#include <vector>
#include <phpcpp.h>

class MainLogic : public Php::Base {
    LA laInitiator;
    PostLAMessaging postLaMessaging;
    uint32_t header_refresh_counter;
    std::string last_header_value;
    int set_up_socket_connect(int port);
    int conduct_la();
    int get_initial_headers();
    int decode_base64_fields_list(std::vector <std::string> &base64_fields_list, std::vector <std::vector<unsigned char>> &binary_fields_list);
public:
    void deployment_stage();
    Php::Value get_mitigator_header();
    Php::Value php_decrypt_wrapper(Php::Parameters &params  );
};


#endif //APACHE_PHP_EXTENSION_MAINLOGIC_H
