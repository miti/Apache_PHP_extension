//
// Created by miti on 2019-12-28.
//

#ifndef APACHE_PHP_EXTENSION_POSTLAMESSAGING_H
#define APACHE_PHP_EXTENSION_POSTLAMESSAGING_H
#include "PostLAMessages.pb.h"
#include "ProtobufMessageRW.h"
#include <string>
#include <vector>
class PostLAMessaging {
    uint8_t key[16];
    ProtobufMessageRW headersChannel;
    ProtobufMessageRW dataChannel;

    uint32_t aes_gcm_wrapper(int enc, uint8_t *plaintext, uint32_t plaintext_length, uint8_t *ciphertext,
                             uint32_t *ciphertext_length);

    uint32_t encrypt_decrypt_msgs(int encrypt_decrypt, std::vector <std::vector<unsigned char>> &input_msgs,
                                  std::vector <std::vector<unsigned char>> &output_msgs);

    void create_vector_from_protobuf(decryptor_to_extension_msg &protobuf_msg,
                                     std::vector <std::vector<unsigned char>> &output_vector);

    void create_protobuf_from_vector(std::vector <std::vector<unsigned char>> &input_vector,
                                     extension_to_decryptor_msg &protobuf_msg);

public:
    void set_la_symmetric_key(uint8_t *given_key);

    void set_headers_fd(int given_fd);

    void set_data_fd(int given_fd);

    uint32_t receive_header_from_decryptor(std::string &output_msg);

    uint32_t receive_data_from_decryptor(std::vector <std::vector<unsigned char>> &output_msgs);

    uint32_t send_data_to_decryptor(std::vector <std::vector<unsigned char>> &plaintext_msg_list);
};
#endif //APACHE_PHP_EXTENSION_POSTLAMESSAGING_H
