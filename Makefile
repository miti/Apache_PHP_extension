CXX             = g++
RM              = rm -f
CXX_FLAGS       = -Wall  -O2 -std=c++11 -fpic
LD              = ${CXX} -v
LD_FLAGS        = -Wall -shared -Wl,--no-undefined

OBJECTS := systemMain.o MainLogic.o LAInitiator/LA.o LAInitiator/Tramsforms.o  ProtobufLAMessages.pb.o PostLA/PostLAMessaging.o PostLA/PostLAMessages.pb.o ProtobufMessageRW.o crypto.o

SGX_SDK := /opt/intel/sgxsdk
SGX_COMMON_CFLAGS := -m64
SGX_LIBRARY_PATH := $(SGX_SDK)/lib64
Trts_Library_Name := sgx_trts
Service_Library_Name := sgx_tservice
Crypto_Library_Name := sgx_tcrypto
SgxC_Library_Name := sgx_tstdc
SGX_INCLUDE_PATHS := -I$(SGX_SDK)/include -I$(SGX_SDK)/include/tlibc -I$(SGX_SDK)/include/libcxx

Uae_Library_Name := sgx_uae_service

all: localattestation_decryption.so #${OBJECTS}

clean:
	${RM} *.obj *~* ${OBJECTS} localattestation_decryption.so

Protobuf%.o: Protobuf%.cpp
	${CXX} -I./include ${CXX_FLAGS} -c $^ -o $@

PostLAMessages.pb.o: PostLAMessages.pb.cpp
	${CXX} -I./include ${CXX_FLAGS} -c $^ -o $@

system%.o: system%.cpp
	g++ -I./include ${CXX_FLAGS} -c $^ -o $@

MainLogic.o: MainLogic.cpp
	g++ -I./include ${CXX_FLAGS} -c $^ -o $@

PostLA/%.o: PostLA/%.cpp
	g++ -I./include -I$(SGX_SDK)/include ${CXX_FLAGS} -c $^ -o $@

LAInitiator/%.o: LAInitiator/%.cpp
	g++ -I./include -I$(SGX_SDK)/include ${CXX_FLAGS} -c $^ -o $@

ProtobufMessageRW.o: ProtobufMessageRW.cpp
	g++ -I./include ${CXX_FLAGS} -c $^ -o $@

crypto.o: crypto.cpp 
	g++ ${CXX_FLAGS} -c $^ -o $@

localattestation_decryption.so: systemMain.o MainLogic.o LAInitiator/LA.o LAInitiator/Tramsforms.o  ProtobufLAMessages.pb.o PostLA/PostLAMessaging.o PostLA/PostLAMessages.pb.o ProtobufMessageRW.o crypto.o
	${CXX} ${LD_FLAGS} -Wl,--verbose systemMain.o MainLogic.o LAInitiator/LA.o LAInitiator/Tramsforms.o  ProtobufLAMessages.pb.o PostLA/PostLAMessaging.o PostLA/PostLAMessages.pb.o ProtobufMessageRW.o crypto.o -lphpcpp -lcrypto -lssl -L$(SGX_LIBRARY_PATH) -l$(Service_Library_Name) -l$(Crypto_Library_Name) -l$(Trts_Library_Name) -l$(SgxC_Library_Name)  -lprotobuf -Wl,-Bsymbolic -Wl,-pie,-eenclave_entry -Wl,--export-dynamic  -o $@
# -lphpcpp -L./ -Wl,--whole-archive -l$(Trts_Library_Name) -Wl,--no-whole-archive  -l$(Service_Library_Name) -l$(Crypto_Library_Name) -lsgx_tstdc -lprotobuf -lssl -lcrypto
#-lcrypto -lssl -L$(SGX_LIBRARY_PATH) -l$(Service_Library_Name) -l$(Crypto_Library_Name) -l$(Trts_Library_Name) -l$(SgxC_Library_Name) -lprotobuf
# -Wl,-Bsymbolic -Wl,-pie,-eenclave_entry -Wl,--export-dynamic  -Wl,--verbose -o $@
